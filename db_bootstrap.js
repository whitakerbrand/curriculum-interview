db = db.getSiblingDB("aggregation_practice")
db.createCollection("players")

db.players.insertMany([
  {
    name: "Sasha the Swordfighter",
    types: ["fighter", "melee"],
    health: 40,
    gold: 403,
    inventory: [
      { item: 'sword', cost: 7 },
      { item: 'armor', cost: 2 },
      { item: 'shield', cost: 1 },
    ]
  },
  {
    name: "Meemo the Mage",
    types: ["magician", "ranged"],
    health: 2,
    gold: 10,
    inventory: [
      { item: 'hat', cost: 3 },
      { item: 'wand', cost: 150 },
      { item: 'spellbook', cost: 17 },
    ]
  },
  {
    name: "Agnor the Archer",
    types: ["fighter", "ranged"],
    health: 32,
    gold: 52,
    inventory: [
      { item: 'bow', cost: 120 },
      { item: 'arrow', cost: 1 },
      { item: 'arrow', cost: 1 },
      { item: 'arrow', cost: 1 },
      { item: 'boots', cost: 5 },
    ]
  },
])

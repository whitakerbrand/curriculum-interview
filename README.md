# MongoDB QL vs Aggregation Pipeline

Lecture slides and student activity for learning the difference between the two.

This tutorial expects that you have a MongoDB and `mongosh` access to it.

You can run `mongo db_bootstrap.js` to get your collection in sync with the tutorial, or just follow the instructions in the slides! :D
